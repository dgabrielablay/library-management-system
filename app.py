from flask import *
import os
import pymongo
from bson.objectid import ObjectId
from bson.binary import Binary
from bson import BSON
import datetime
from werkzeug.utils import secure_filename
from PIL import Image
from io import BytesIO

DB_NAME = 'heroku_6q3vw2q0'  
DB_HOST = 'ds257054.mlab.com'
DB_PORT = 57054
DB_USER = 'admin' 
DB_PASS = 'password123'

myclient = pymongo.MongoClient(DB_HOST, DB_PORT)
mydb = myclient[DB_NAME]
mydb.authenticate(DB_USER, DB_PASS)
mystudents = mydb['students']
mybooks = mydb['books']
myreservations = mydb['reservations']

UPLOAD_FOLDER = 'static/bookcovers'
ALLOWED_EXTENSIONS = ['png', 'jpg', 'jpeg']

app = Flask(__name__)
app.secret_key = 'lms-secret-key'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

class ADMIN:
    ID = '12007'
    PASS = 'root'
    TYPE = 'admin'

@app.context_processor
def override_url_for():
    return dict(url_for=dated_url_for)

def dated_url_for(endpoint, **values):
    try:
        if endpoint == 'static':
            filename = values.get('filename', None)
            if filename:
                file_path = os.path.join(app.root_path,
                                        endpoint, filename)
                values['q'] = int(os.stat(file_path).st_mtime)
        return url_for(endpoint, **values)
    except FileNotFoundError:
        return url_for("static", filename='bookcovers/the-lord-of-the-rings-book-cover.jpg')

##
# PAGES
##

@app.route('/')
def home():
    return render_template('landing.html')

@app.route('/login')
def login_page():
    return render_template('login.html')

@app.route('/login/submit', methods=['POST', 'GET'])
def login():
    try:
        if(request.method == 'POST'): 
            stud_id = request.form['id']
            stud_password = request.form['password']
            if(stud_id == ADMIN.ID and stud_password == ADMIN.PASS):
                session['type'] = ADMIN.TYPE
                session['id'] = ADMIN.ID
                session['new'] = False
                custom_flash('You have successfully logged in!')
                return redirect(url_for('home'))
            elif(len(list(mystudents.find({'id': stud_id, 'password': stud_password}))) > 0):
                session['type'] = 'user'
                session['id'] = stud_id
                session['new'] = (False, True)[stud_password == 'password']
                custom_flash('You have successfully logged in!')
                return redirect(url_for('home'))
            else:
                custom_flash('Invalid id/password. Try again.')
    except Exception as e:
        custom_flash(str(e))
    return redirect(url_for('home'))        

@app.route('/logout')
def logout():
    if('type' in session):
        session.pop('id', None)
        session.pop('type', None)
        session.pop('new', None)
        custom_flash('You have successfully logged out.')
    return redirect(url_for('home'))

@app.route('/books/reserve/<bookid>')
def reserve(bookid):
    if(check_if_session_is_active()):
        update_book_status(bookid, BOOK_STATUS.RESERVED)
        reservation = {
            'userid': session['id'],
            'bookid': bookid,
            'status': BOOK_STATUS.RESERVED
        }
        x = []
        try:
            x = list(myreservations.find_one(reservation))
            custom_flash('That book is already reserved.')
        except:
            try:
                x = list(myreservations.find({'userid': session['id'], 'status': BOOK_STATUS.RESERVED}))
                if(len(x) == 2):
                    custom_flash('You have already reserved 2 books.')
                    url = ('user_book', 'admin_book')[check_if_admin()]
                    return redirect(url_for(url, bookid=bookid))
            except:
                """"""
            reservation['timestamp'] = get_current_timestamp()
            reservation['debt'] = None
            debt = compute_debt(session['id'])
            if(debt > 4):
                custom_flash('You cannot reserve anymore books due to overdue fees.')
                url = ('user_book', 'admin_book')[check_if_admin()]
                return redirect(url_for(url, bookid=bookid))
            x = myreservations.insert_one(reservation)
            custom_flash('Successfully reserved that book. Wait for the librarian to confirm your reservation.')
            url = ('user_reservation', 'admin_reservation')[check_if_admin()]
            return redirect(url_for(url, rid=x.inserted_id))
    return redirect(url_for("home"))

@app.route('/reservations/confirm/<rid>')
def confirm(rid):
    if(check_if_admin()):
        reservation = get_reservation_details(rid)
        x = []
        try:
            x = list(myreservations.find({'userid': reservation['userid'], 'status': BOOK_STATUS.BORROWED}))
        except:
            """"""
        if(len(x) == 5):
            custom_flash('That user already has 5 books borrowed.')
            update_book_status(reservation['bookid'], BOOK_STATUS.ONSHELF)
            update_reservation_status(rid, BOOK_STATUS.ONSHELF)
        else:
            update_book_status(reservation['bookid'], BOOK_STATUS.BORROWED)
            update_reservation_status(rid, BOOK_STATUS.BORROWED)
            custom_flash('You have successfully lent that book for that user.')
        return redirect(url_for('admin_reservation', rid=rid))
    return redirect(url_for("home"))

@app.route('/reservations/return/<rid>')
def return_book(rid):
    if(check_if_admin()):
        reservation = get_reservation_details(rid)
        update_book_status(reservation['bookid'], BOOK_STATUS.ONSHELF)
        update_reservation_status(rid, BOOK_STATUS.RETURN)
        custom_flash('You have successfully returned that book.')
        debt = compute_debt(reservation['userid'], rid)
        myreservations.update_one(
            {'_id': ObjectId(rid)},
            {'$set': {
                'debt': debt
            }}
        )
        return redirect(url_for('admin_reservation', rid=rid))
    return redirect(url_for("home"))

@app.route('/changepassword', methods=['POST', 'GET'])
def changepassword():
    if(check_if_session_is_active()):
        if(request.method == 'POST'):
            message = ''
            old_pass = request.form['old_pass']
            new_pass = request.form['new_pass']
            confirm_new_pass = request.form['confirm_new_pass']
            user = mystudents.find_one({'id': session['id']})
            if(old_pass != user['password']):
                message = 'Old password does not match your password'
            elif(new_pass != confirm_new_pass):
                message = 'New password does not match the confirmation password'
            elif(len(new_pass) < 8):
                message = 'New password must be atleast 8 characters'
            elif(new_pass == 'password'):
                message = 'New password cannot be "password"'
            else:
                mystudents.update_one(
                    {'id': session['id']},
                    {'$set': {
                        'password': new_pass
                    }}
                )
                message = 'Successfully changed password'
            return Response(json.dumps({'message': message}), mimetype='application/json')
    return redirect(url_for('home'))

##
# ADMIN PAGES
##

##
# ADMIN - HOME
##
@app.route('/admin')
def admin():
    if(not check_if_admin()):
        return redirect(url_for('home'))
    return render_template('admin/index.html')

##
# ADMIN - STUDENTS
##
@app.route('/admin/students')
def students():
    if(not check_if_admin()):
        return redirect(url_for('home'))
    students = [student for student in mystudents.find().sort('last')]
    return render_template('admin/students.html', students=students)

##
# ADMIN - VIEW STUDENT
##
@app.route('/admin/students/<studid>')
def student(studid):
    if(not check_if_admin()):
        return redirect(url_for('home'))
    student = mystudents.find_one({'id': studid})
    return render_template('admin/student.html', student=student)

##
# ADMIN - EDIT STUDENT
##
@app.route('/admin/students/edit', methods=['POST', 'GET'])
def edit_student():
    if(not check_if_admin()):
        return redirect(url_for('home'))
    if(request.method == 'POST'):
        first = request.form['first']
        last = request.form['last']
        idd = request.form['id']
        _id = request.form['_id']
        prev_id = request.form['prev_id']
        mystudents.update_one(
            {'_id': ObjectId(_id)},
            {'$set': {
                'first': first,
                'last': last,
                'id': idd
            }}
        )
        myreservations.update(
            {'userid': prev_id},
            {'$set': {
                'userid': idd
            }}
        )
        custom_flash('You have successfully edited that student')
        return redirect(url_for('student', studid=idd))
    return redirect(url_for('home'))

##
# ADMIN - ADD STUDENT
##
@app.route('/admin/students/new')
def new_student():
    if(not check_if_admin()):
        return redirect(url_for('home'))
    return render_template('admin/add_student.html')

##
# ADMIN - ACTION ADD STUDENT
##
@app.route('/admin/students/new/submit', methods=['POST', 'GET'])
def new_student_submit():
    if(not check_if_admin()):
        return redirect(url_for('home'))
    if(request.method == 'POST'):
        student_id = request.form['id']
        if(student_id == '12007'):
            custom_flash(f'That student already exists.')
            return redirect(url_for('new_student'))
        students = list(mystudents.find({'id': student_id}))
        if(len(students) == 0):
            mydict = {
                'id': student_id,
                'first': request.form['first'],
                'last': request.form['last'],
                'password': 'password'
            }
            x = mystudents.insert_one(mydict)
            custom_flash(f'You have added a new student.')
            return redirect(url_for('students'))
        else:
            custom_flash(f'That student already exists.')
            return redirect(url_for('new_student'))

##
# ADMIN - DELETE STUDENT
##
@app.route('/admin/students/delete/<studid>')
def delete_student(studid):
    if(not check_if_admin()):
        return redirect(url_for('home'))
    student = {'id': studid}
    mystudents.delete_one(student)
    try:
        myreservations.delete({'userid': studid})
    except:
        """"""
    return redirect(url_for('students'))

##
# ADMIN - BOOKS
##
@app.route('/admin/books')
def admin_books():
    if(not check_if_admin()):
        return redirect(url_for('home'))
    books_with_sorted_categories = {}
    categories = genres
    for category in categories:
        try:
            books_with_sorted_categories[category] = [book for book in mybooks.find({'category': category})]
        except Exception as e:
            print(f'{category} {e}')
    return render_template(
        'books.html',
        books=books_with_sorted_categories,
        categories=categories,
        len=len
    )

##
# ADMIN - ADD BOOK
##
@app.route('/admin/books/new')
def new_book():
    if(not check_if_admin()):
        return redirect(url_for('home'))
    return render_template(
        'admin/add_book.html',
        categories=genres,
        year=get_current_year()
    )

##
# ADMIN - ACTION ADD BOOK
##
@app.route('/admin/books/new/submit', methods=['POST', 'GET'])
def new_book_submit():
    if(not check_if_admin()):
        return redirect(url_for('home'))
    if(request.method == 'POST'):
        book_title = request.form['title']
        isbn = request.form['isbn']
        copy_no = len([book for book in mybooks.find({'isbn': isbn}, {'_id': 1})]) + 1
        _id = f'{request.form["category"]}{isbn[:-5]}__{copy_no}'
        mydict = {
            'id': _id,
            'isbn': isbn,
            'title': book_title,
            'author': request.form['author'],
            'year': request.form['year'],
            'copy_no': copy_no,
            'category': request.form['category'],
            'status': BOOK_STATUS.ONSHELF
        }
        x = mybooks.insert_one(mydict)
        if 'file' not in request.files:
            flash('No file part')
        file = request.files['file']
        if file.filename == '':
            flash('No selected file')
        if(file and check_if_allowed_file(file.filename)):
            filename = secure_filename(f'{x.inserted_id}.png')
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            filename = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            with open(filename, 'rb') as f:
                encoded = Binary(f.read())
                mybooks.update_one(
                    {'_id': ObjectId(x.inserted_id)},
                    {'$set': {'bookcover': encoded}}
                )
        custom_flash(f'You have added a new book.')
        return redirect(url_for('admin_books'))

##
# ADMIN - VIEW BOOK
##
@app.route('/admin/books/<bookid>')
def admin_book(bookid):
    if(not check_if_admin()):
        return redirect(url_for('home'))
    book = mybooks.find_one({'_id': ObjectId(bookid)})
    if(len(list(book)) == 0):
        return redirect(url_for('admin_books'))
    filename = f"{os.path.join(app.config['UPLOAD_FOLDER'], '{}.png'.format(book['_id']))}"
    if(not os.path.isfile(filename)):
        try:
            img = binToImage(book['bookcover'])
            img.save(filename)
        except KeyError as ke:
            """"""
        except Exception as e:
            print(e)
    reservation = myreservations.find_one({'bookid': bookid}, {'userid': 1})
    return render_template(
        'book.html',
        book=book,
        c=get_category,
        reservation=reservation
    )

##
# ADMIN - EDIT BOOK
##
@app.route('/admin/books/edit', methods=['POST', 'GET'])
def edit_book():
    if(not check_if_admin()):
        return redirect(url_for('home'))
    if(request.method == 'POST'):
        author = request.form['author']
        isbn = request.form['isbn']
        year = request.form['year']
        idd = request.form['id']
        bookid = request.form['bookid']
        mybooks.update_one(
            {'_id': ObjectId(bookid)},
            {'$set': {
                'author': author,
                'isbn': isbn,
                'year': year,
                'id': idd
            }}
        )
    return Response(json.dumps({'': []}), mimetype='application/json')

##
# ADMIN - DELETE BOOK
##
@app.route('/admin/books/delete/<bookid>')
def delete_book(bookid):
    if(not check_if_admin()):
        return redirect(url_for('home'))
    book = {'_id': ObjectId(bookid)}
    mybooks.delete_one(book)
    try:
        myreservations.delete({'bookid': bookid})
    except:
        """"""
    return redirect(url_for('admin_books'))

##
# ADMIN - UPDATE BOOK COVER
##
@app.route('/admin/books/newbookcover/<bookid>', methods=['POST', 'GET'])
def admin_update_bc(bookid):
    if(not check_if_admin()):
        return redirect(url_for('home'))
    if(request.method == 'POST'):
        if 'file' not in request.files:
            flash('No file part')
        file = request.files['file']
        if file.filename == '':
            flash('No selected file')
        if(file and check_if_allowed_file(file.filename)):
            filename = secure_filename(f'{bookid}.png')
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            filename = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            with open(filename, 'rb') as f:
                encoded = Binary(f.read())
                mybooks.update_one(
                    {'_id': ObjectId(bookid)},
                    {'$set': {'bookcover': encoded}}
                )
    return redirect(url_for('admin_book', bookid=bookid))

##
# ADMIN - RESERVATIONS
##
@app.route('/admin/reservations')
def admin_reservations():
    if(not check_if_admin()):
        return redirect(url_for('home'))
    reservations = [reservation for reservation in myreservations.find().sort('timestamp', -1)]
    return render_template(
        'reservations.html',
        reservations=reservations,
        title=get_book_title,
        get_year_from_timestamp=get_year_from_timestamp
    )

##
# ADMIN - VIEW RESERVATION
##
@app.route('/admin/reservations/<rid>')
def admin_reservation(rid):
    if(not check_if_admin()):
        return redirect(url_for('home'))
    reservation = myreservations.find_one({'_id': ObjectId(rid)})
    return render_template(
        'reservation.html',
        reservation=reservation,
        book=get_book_details(reservation['bookid']),
        user=get_user_details(reservation['userid']),
        c=get_category,
        get_year_from_timestamp=get_year_from_timestamp,
        BOOK_STATUS=BOOK_STATUS
    )

##
# STUDENT PAGES
##

##
# STUDENT - BOOKS
##
@app.route('/books')
def user_books():
    if(not check_if_user()):
        return redirect(url_for('home'))
    books_with_sorted_categories = {}
    categories = genres
    for category in categories:
        books_with_sorted_categories[category] = [book for book in mybooks.find({'category': category})]
        for book in books_with_sorted_categories[category]:
            filename = f"{os.path.join(app.config['UPLOAD_FOLDER'], '{}.png'.format(book['_id']))}"
            if(not os.path.isfile(filename)):
                try:
                    img = binToImage(book['bookcover'])
                    img.save(filename)
                except KeyError as ke:
                    """"""
                except Exception as e:
                    print(e)
    return render_template(
        'books.html',
        books=books_with_sorted_categories,
        categories=categories,
        len=len
    )

##
# STUDENT - VIEW BOOK
##
@app.route('/books/<bookid>')
def user_book(bookid):
    if(not check_if_user()):
        return redirect(url_for('home'))
    book = mybooks.find_one({'_id': ObjectId(bookid)})
    if(len(list(book)) == 0):
        return redirect(url_for('user_books'))
    reservation = myreservations.find_one({'bookid': bookid}, {'userid': 1})
    return render_template(
        'book.html',
        book=book,
        len=len,
        decode=binToImage,
        c=get_category,
        reservation=reservation
    )

##
# STUDENT - RESERVATIONS
##
@app.route('/reservations')
def user_reservations():
    if(not check_if_user()):
        return redirect(url_for('home'))
    print(session['id'])
    reservations = [reservation for reservation in myreservations.find({'userid': session['id']}, {'_id': 1, 'bookid': 1}).sort('timestamp', -1)]
    for reservation in reservations:
        book = get_book_details(reservation['bookid'])
        print(book)
        try:
            filename = f"{os.path.join(app.config['UPLOAD_FOLDER'], '{}.png'.format(book['_id']))}"
            if(not os.path.isfile(filename)):
                try:
                    img = binToImage(book['bookcover'])
                    img.save(filename)
                except KeyError as ke:
                    """"""
                except Exception as e:
                    print(e)
        except Exception as e:
            print(e)
    return render_template(
        'reservations.html',
        reservations=reservations,
        get_book_details=get_book_details,
        get_user_details=get_user_details,
        categories=genres,
        get_category=get_category,
        if_category_has_reservations=if_category_has_reservations,
        len=len
    )

##
# STUDENT - ADD RESERVATION
##
@app.route('/reservations/new')
def new_reservation():
    if(not check_if_user()):
        return redirect(url_for('home'))
    books = [book for book in mybooks.find()]
    return render_template(
        'student/add_reservation.html',
        books=books
    )

##
# STUDENT - VIEW RESERVATION
##
@app.route('/reservations/<rid>')
def user_reservation(rid):
    if(not check_if_user()):
        return redirect(url_for('home'))
    reservation = myreservations.find_one({'_id': ObjectId(rid)})
    return render_template(
        'reservation.html',
        reservation=reservation,
        book=get_book_details(reservation['bookid']),
        user=get_user_details(reservation['userid']),
        c=get_category,
        get_year_from_timestamp=get_year_from_timestamp,
        BOOK_STATUS=BOOK_STATUS
    )


##
# UTILITIES
##

@app.route('/requirements')
def requirements():
    return render_template('requirements.html')

def check_if_session_is_active():
    if('type' in session):
        return True
    return False

def check_if_user():
    if(check_if_session_is_active()):
        if(session['type'] == 'user'):
            return True
    return False

def check_if_admin():
    if(check_if_session_is_active()):
        if(session['type'] == ADMIN.TYPE):
            return True
    return False

def custom_flash(message):
    flash(message)

def get_current_year():
    return f'{datetime.datetime.now()}'[0:4]

def get_current_timestamp():
    return f'{datetime.datetime.now()}'

def get_year_from_timestamp(timestamp):
    return f'{timestamp}'.split(' ')[0]

def get_file_extension(filename):
    fliename = filename.split('.')
    return fliename[len(fliename)-1].lower()

def check_if_allowed_file(filename):
    return '.' in filename and get_file_extension(filename) in ALLOWED_EXTENSIONS

def binToImage(binary):
    return Image.open(BytesIO(binary))

class BOOK_STATUS:
    ONSHELF = 'onshelf'
    ONLOAN = 'onloan'
    ONHOLD = 'onhold'
    ONLHOLD = 'onloanandonhold'
    REQUEST = 'request'
    WITHDRAW = 'withdraw'
    RETURN = 'return'
    RESERVED = 'reserved'
    BORROWED = 'borrowed'

genres = {
    'A': 'General Works',
    'B': 'Philosophy, Psychology, and Religion',
    'C': 'History - Civilization',
    'D': 'History - General',
    'E': 'Philippine History',
    'F': 'Philippine Local History',
    'G': 'Geography, Anthropology, and Recreation',
    'H': 'Social Sciences',
    'J': 'Political Sciences',
    'K': 'Law',
    'L': 'Education',
    'M': 'Music',
    'N': 'Fine Arts',
    'P': 'Language and Literature',
    'O': 'Fiction',
    'Q': 'Science',
    'R': 'Medicine',
    'S': 'Agriculture',
    'T': 'Technology',
    'Z': 'Information Science',
}

def get_category(category_id):
    return genres[category_id]

def update_book_status(bookid, status):
    mybooks.update_one(
        {'_id': ObjectId(bookid)},
        {'$set': {
            'status': status
        }}
    )

def update_reservation_status(rid, status):
    myreservations.update_one(
        {'_id': ObjectId(rid)},
        {'$set': {
            'status': status,
            'timestamp': get_current_timestamp()
        }}
    )

def get_reservation_details(rid):
    return myreservations.find_one({'_id': ObjectId(rid)})

def get_book_title(bookid):
    return mybooks.find_one({'_id': ObjectId(bookid)})['title']

def get_book_details(bookid):
    return mybooks.find_one({'_id': ObjectId(bookid)})

def get_user_details(userid):
    return mystudents.find_one({'id': userid})

def if_category_has_reservations(category, reservations):
    for reservation in reservations:
        book = get_book_details(reservation['bookid'])
        if(book['category'] == category):
            return True
    return False

DEBT_MULTIPLIER = 8

def compute_debt(userid, rid=None):
    reservation123 = {'userid': userid, 'status': BOOK_STATUS.BORROWED}
    if(rid != None):
        reservation123 = {'_id': ObjectId(rid)}
    reservations1 = myreservations.find(reservation123)
    debt = 0
    now = datetime.datetime.strptime(get_current_timestamp(), '%Y-%m-%d %H:%M:%S.%f')
    for reservation1 in reservations1:
        date = datetime.datetime.strptime(reservation1['timestamp'], '%Y-%m-%d %H:%M:%S.%f')
        days = now - date
        days = days.total_seconds()
        days = divmod(days, 86400)[0]
        debt += (days * DEBT_MULTIPLIER)
    return debt

if(__name__ == '__main__'):
    app.run('0.0.0.0', debug=True)